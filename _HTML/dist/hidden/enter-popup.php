
<div class="mfiModal zoomAnim enterPopup">
	<div class="regPopup__header">
		<div class="regPopup__logo">
			<div class="topWhiteLine__logo">
				<div class="topWhiteLine__logo__wrapper">
					<div class="topWhiteLine__logo__wrapper__border">
						<div class="svgHolder">
							<svg>
								<use xlink:href="#logo"/>
							</svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="regPopup__textBlock">
		<div class="regPopup__title mainTitle_green w_tac">Авторизация</div>
		<div data-form="true" class="wForm wFormDef wFormPreloader myForm">
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="email" name="demo_email_input" id="demo_email_input" placeholder="Введите E-mail" data-rule-email="true"/>
					<div class="inpInfo">E-mail *</div>
				</div>
			</div>
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="password" name="demo_date_input" id="demo_date_input" placeholder="Введите пароль" data-rule-password="true" data-rule-minlength='4'/>
					<div for="demo_date_input" class="inpInfo">Пароль *</div>
				</div>
			</div>
			<div class="wFormRow">
				<label class="wCheck">
					<input class="wInput" type="checkbox" name="demo_required_checkbox"/><ins>&nbsp;</ins><span>Запомнить меня</span>
				</label>
			</div>
			<div class="wFormRow w_last">
				<button class="wSubmit button_aqua button_semi">Войти</button>
			</div>
		</div>
		<div data-url="hidden/remember-popup.php" class="enterPopup__remember mfiA">Забыли пароль?</div>
		<div class="enterPopup__new">Новый пользователь?<span data-url="hidden/reg-popup.php" class="mfiA">Регистрация</span></div>
		<div class="enterPopup__or"></div>
		<div class="regPopup__facebook">
			<p>Войти как пользователь</p>
			<div class="fb"></div>
		</div>
	</div>
</div>