
<div class="mfiModal zoomAnim cristmas-popup">
	<div class="cristmas-popup__headrer"><img src="pic/popup-head.jpg"/>
		<div class="cristmas-popup__logo">
			<div class="topWhiteLine__logo">
				<div class="topWhiteLine__logo__wrapper">
					<div class="topWhiteLine__logo__wrapper__border">
						<div class="svgHolder">
							<svg>
								<use xlink:href="#logo"/>
							</svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="cristmas-popup__title mainTitle_green w_tac">Рождественные праздники не за горами?</div>
	<div class="cristmas-popup__main">
		<div class="cristmas-popup__left"><img src="pic/cristmas_left.jpg"/></div>
		<div class="cristmas-popup__center">
			<div class="cristmas-popup__smallTitle">YouChef позаботится о Вас  и о Вашей семье!</div>
			<hr/>
			<div class="cristmas-popup__text">Эксклюзивные блюда по случаю наступающего Нового года и Рождества  специально были разработаны нашими</div>
			<div class="cristmas-popup__button"><a href="#" class="button_semi button_aqua">Хочу попробовать!</a></div>
		</div>
		<div class="cristmas-popup__right"><img src="pic/cristmas_left.jpg"/></div>
	</div>
</div>