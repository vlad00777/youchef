
<div class="mfiModal zoomAnim popupRecipe box">
	<div style="background-image:url(pic/recipehead.jpg)" class="popupRecipe__top">
		<div class="popupRecipe__logo">
			<div class="topWhiteLine__logo">
				<div class="topWhiteLine__logo__wrapper">
					<div class="topWhiteLine__logo__wrapper__border">
						<div class="svgHolder">
							<svg>
								<use xlink:href="#logo"/>
							</svg>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="popupRecipe__socials">
			<div class="footerBlock__socials small"><a href="#" class="facebook"><span class="svgHolder">
						<svg>
							<use xlink:href="#facebook"/>
						</svg></span></a><a href="#" class="twitter"><span class="svgHolder">
						<svg>
							<use xlink:href="#twitter"/>
						</svg></span></a><a href="#" class="instagram"><span class="svgHolder">
						<svg>
							<use xlink:href="#instagram"/>
						</svg></span></a><a href="#" class="google"><span class="svgHolder">
						<svg>
							<use xlink:href="#google"/>
						</svg></span></a><a href="#" class="youtube"><span class="svgHolder">
						<svg>
							<use xlink:href="#youtube"/>
						</svg></span></a></div>
			<div class="popupRecipe__drop"><span>Поделись мастерством</span></div>
		</div>
		<div class="popupRecipe__title"><span>Говядина в сливочном соусе с морковью</span><a href="#" class="archiveBlock__download">
				<div class="icon">
					<div class="svgHolder">
						<svg>
							<use xlink:href="#download"/>
						</svg>
					</div>
				</div><span>Скачать рецепт</span></a></div>
	</div>
	<div class="popupRecipe__main">
		<div class="popupRecipe__infoLine">
			<div class="popupRecipe__infoLine__title">Питательность</div>
			<div class="popupRecipe__infoLine__count"><b>33 г</b> углеводов <b>12 г</b> жира <b>19 г</b> белка <b>1009 мг</b> натрия</div>
			<div class="popupRecipe__infoLine__desc">низкая калорийность, низкое содержание углеводов, без глютена, без молока, без сои, без ореха</div>
		</div>
		<div class="popupRecipe__bigLine">
			<div class="popupRecipe__bigLine__kalories popupRecipe__bigLine__flex">
				<div class="popupRecipe__bigLine__icon apple"></div>
				<div class="popupRecipe__bigLine__nc">
					<div class="popupRecipe__bigLine__name">Калории</div>
					<div class="popupRecipe__bigLine__count">296</div>
				</div>
			</div>
			<div class="popupRecipe__bigLine__time popupRecipe__bigLine__flex">
				<div class="popupRecipe__bigLine__icon time"></div>
				<div class="popupRecipe__bigLine__nc">
					<div class="popupRecipe__bigLine__name">Время приготовления</div>
					<div class="popupRecipe__bigLine__count">30-40 минут</div>
				</div>
			</div>
			<div class="popupRecipe__bigLine__expiration popupRecipe__bigLine__flex">
				<div class="popupRecipe__bigLine__icon ex"></div>
				<div class="popupRecipe__bigLine__nc">
					<div class="popupRecipe__bigLine__name">Срок годности</div>
					<div class="popupRecipe__bigLine__count">3 дня</div>
				</div>
			</div>
			<div class="popupRecipe__bigLine__hard popupRecipe__bigLine__flex">
				<div class="popupRecipe__bigLine__icon">
					<div data-rating="1" class="popupRecipe__bigLine__cook js-popupRecipe">
						<div class="popupRecipe__bigLine__cookIcon js-icon"><span class="svgHolder">
								<svg>
									<use xlink:href="#logo"/>
								</svg></span></div>
						<div class="popupRecipe__bigLine__cookIcon js-icon"><span class="svgHolder">
								<svg>
									<use xlink:href="#logo"/>
								</svg></span></div>
						<div class="popupRecipe__bigLine__cookIcon js-icon"><span class="svgHolder">
								<svg>
									<use xlink:href="#logo"/>
								</svg></span></div>
					</div>
				</div>
				<div class="popupRecipe__bigLine__nc">
					<div class="popupRecipe__bigLine__name">Сложность</div>
					<div class="popupRecipe__bigLine__count">Легкий</div>
				</div>
			</div>
			<div class="popupRecipe__bigLine__level popupRecipe__bigLine__flex">
				<div data-rating="2" class="popupRecipe__bigLine__icon js-popupRecipe">
					<div class="popupRecipe__bigLine__starIcon js-icon"><span class="svgHolder">
							<svg>
								<use xlink:href="#peper"/>
							</svg></span></div>
					<div class="popupRecipe__bigLine__starIcon js-icon"><span class="svgHolder">
							<svg>
								<use xlink:href="#peper"/>
							</svg></span></div>
					<div class="popupRecipe__bigLine__starIcon js-icon"><span class="svgHolder">
							<svg>
								<use xlink:href="#peper"/>
							</svg></span></div>
				</div>
				<div class="popupRecipe__bigLine__nc">
					<div class="popupRecipe__bigLine__name">Острота</div>
					<div class="popupRecipe__bigLine__count">Средняя</div>
				</div>
			</div>
		</div>
		<div class="popupRecipe__ingredients">
			<div class="ourBlock__composition">
				<div class="ourBlock__image">
					<div style="background-image:url(pic/smuzi-image.jpg)" class="ourBlock__img"></div>
				</div>
				<div class="ourBlock__text">
					<div class="ourBlock__composition__title">Ингридиенты на 2-х человек</div>
					<div class="ourBlock__composition__table">
						<table>
							<tr>
								<td>Стебель сельдерея</td>
								<td><b>20 гр</b></td>
							</tr>
							<tr>
								<td>Стебель сельдерея</td>
								<td><b>20 гр</b></td>
							</tr>
							<tr>
								<td>Стебель сельдерея</td>
								<td><b>20 гр</b></td>
							</tr>
							<tr>
								<td>Стебель сельдерея</td>
								<td><b>20 гр</b></td>
							</tr>
							<tr>
								<td>Стебель сельдерея</td>
								<td><b>20 гр</b></td>
							</tr>
							<tr>
								<td>Базилик сушеный молотый</td>
								<td><b>400 гр</b></td>
							</tr>
							<tr>
								<td>Базилик сушеный молотый</td>
								<td><b>400 гр</b></td>
							</tr>
							<tr>
								<td>Базилик сушеный молотый</td>
								<td><b>400 гр</b></td>
							</tr>
							<tr>
								<td>Базилик сушеный молотый</td>
								<td><b>400 гр</b></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="popupRecipe__quote">
			<blockquote>Данное блюдо готовится очень быстро и на самом сильном огне - это основной принцип приготовления стир-фрай. Когда Вы добавляете лапшу - хорошенько перемешайте блюдо в сковороде, чтобы все ингридиенты и соус равномерно распределить</blockquote>
		</div>
		<div class="popupRecipe__steps">
			<div class="popupRecipe__steps__title w_tac">Способ приготовления</div>
			<div class="popupRecipe__step">
				<div class="popupRecipe__step__image">
					<div class="popupRecipe__step__img"><img src="pic/block-party.jpg"/></div>
				</div>
				<div class="popupRecipe__step__text">Картофель очистите, нарежьте крупными произвольными ломтиками, добавьте в кипящую воду и варите в течение 20 минут. На сухой сковороде обжарьте, помешивая, орехи на сильном огне в течение 30 секунд, снимите с огня. Курагу замочите в 50 мл теплой воды на 5 минут.</div>
			</div>
			<div class="popupRecipe__step">
				<div class="popupRecipe__step__image">
					<div class="popupRecipe__step__img"><img src="pic/block-party.jpg"/></div>
				</div>
				<div class="popupRecipe__step__text">Картофель очистите, нарежьте крупными произвольными ломтиками, добавьте в кипящую воду и варите в течение 20 минут. На сухой сковороде обжарьте, помешивая, орехи на сильном огне в течение 30 секунд, снимите с огня. Курагу замочите в 50 мл теплой воды на 5 минут.</div>
			</div>
			<div class="popupRecipe__step">
				<div class="popupRecipe__step__image">
					<div class="popupRecipe__step__img"><img src="pic/block-party.jpg"/></div>
				</div>
				<div class="popupRecipe__step__text">Картофель очистите, нарежьте крупными произвольными ломтиками, добавьте в кипящую воду и варите в течение 20 минут. На сухой сковороде обжарьте, помешивая, орехи на сильном огне в течение 30 секунд, снимите с огня. Курагу замочите в 50 мл теплой воды на 5 минут.</div>
			</div>
		</div>
		<div class="popupRecipe__footer">
			<div class="popupRecipe__footer__warning">Обратите внимание, что из-за производственных процессов, используемых и нашими поставщиками, наши ящики могут содержать мелкие следы следующих аллергенов в дополнение к аллергенам говорится выше, и на наши рецепты карты: сельдерей, ракообразные, яйца, рыба, глютен, лактозу, люпин, моллюски, орехи (в том числе кедровые орехи, арахис, кунжут, соя, сульфитов и диоксида серы.</div>
		</div>
	</div>
</div>