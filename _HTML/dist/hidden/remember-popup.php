
<div class="mfiModal zoomAnim rememberPopup">
	<div class="regPopup__header">
		<div class="regPopup__logo">
			<div class="topWhiteLine__logo">
				<div class="topWhiteLine__logo__wrapper">
					<div class="topWhiteLine__logo__wrapper__border">
						<div class="svgHolder">
							<svg>
								<use xlink:href="#logo"/>
							</svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="regPopup__textBlock">
		<div class="regPopup__title mainTitle_green w_tac">Забыли пароль?</div>
		<div data-form="true" class="wForm wFormDef wFormPreloader myForm">
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="email" name="demo_email_input" id="demo_email_input" placeholder="E-mail" data-rule-email="true"/>
					<div class="inpInfo">E-mail *</div>
				</div>
			</div>
			<div class="wFormRow w_last">
				<button class="wSubmit button_aqua button_semi">Отправить</button>
			</div>
		</div>
	</div>
</div>