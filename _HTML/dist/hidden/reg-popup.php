
<div class="mfiModal zoomAnim regPopup">
	<div class="regPopup__header">
		<div class="regPopup__logo">
			<div class="topWhiteLine__logo">
				<div class="topWhiteLine__logo__wrapper">
					<div class="topWhiteLine__logo__wrapper__border">
						<div class="svgHolder">
							<svg>
								<use xlink:href="#logo"/>
							</svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="regPopup__textBlock">
		<div class="regPopup__title mainTitle_green w_tac">Регистрация</div>
		<div data-form="true" class="wForm wFormDef wFormPreloader myForm">
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="text" name="demo_word_input" id="demo_word_input" placeholder="Ваше имя" data-rule-word="true"/>
					<div class="inpInfo">Имя *</div>
				</div>
			</div>
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="text" name="demo_word_input2" id="demo_word_input2" placeholder="Ваша фамилия" data-rule-word="true"/>
					<div class="inpInfo">Фамилия *</div>
				</div>
			</div>
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="email" name="demo_email_input" id="demo_email_input" placeholder="E-mail" data-rule-email="true"/>
					<div class="inpInfo">E-mail *</div>
				</div>
			</div>
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="password" name="demo_date_input" id="demo_date_input" placeholder="Пароль" data-rule-password="true" data-rule-minlength='4'/>
					<div for="demo_date_input" class="inpInfo">Пароль *</div>
				</div>
			</div>
			<div class="wFormRow calendar">
				<label for="demo_required_input" class="wLabel">Дата рождения</label>
				<div class="wFormInput">
					<input class="wInput" type="text" name="demo_required_input" id="demo_required_input" placeholder="" data-rule-mydate="data-rule-mydate"/>
					<div class="inpInfo">Дата рождения</div>
					<div class="svgHolder">
						<svg>
							<use xlink:href="#date"/>
						</svg>
					</div>
				</div>
			</div>
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput js-phone-mask" required="required" type="tel" name="demo_phoneua_input" id="demo_phoneua_input" placeholder="Номер телефона" data-rule-phoneUA="true"/>
					<div for="demo_phoneua_input" class="inpInfo">Телефон *</div>
				</div>
			</div>
			<div class="wFormRow">
				<div class="wFormSelect">
					<select required="required" class="wSelect">
						<option value="0" disabled="disabled" selected="selected">Город</option>
						<option value="1">Херсон</option>
					</select>
				</div>
			</div>
			<div class="wFormRow w_last">
				<button class="wSubmit button_aqua button_semi">Регистрация</button>
			</div>
		</div>
		<div class="regPopup__facebook">
			<p>Зарегистрироваться через</p>
			<div class="fb"></div>
		</div>
	</div>
</div>