
<div class="mfiModal zoomAnim pricePopup">
	<div class="regPopup__header">
		<div class="regPopup__logo">
			<div class="topWhiteLine__logo">
				<div class="topWhiteLine__logo__wrapper">
					<div class="topWhiteLine__logo__wrapper__border">
						<div class="svgHolder">
							<svg>
								<use xlink:href="#logo"/>
							</svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="regPopup__textBlock">
		<div class="regPopup__title mainTitle_green w_tac">Наши цены</div>
		<div class="pricePopup__table">
			<div class="pricePopup__row">
				<div class="pricePopup__desc"><span><b class="humans">2</b></span><img src="pic/popup-human.png"/><span>x&nbsp;&nbsp;<b class="count">3</b></span><img src="pic/popup-small-pizza.png"/><span>= £<b class="total">35</b></span></div>
			</div>
			<div class="pricePopup__row">
				<div class="pricePopup__desc"><span><b class="humans">2</b></span><img src="pic/popup-human.png"/><span>x&nbsp;&nbsp;<b class="count">3</b></span><img src="pic/popup-small-pizza.png"/><span>= £<b class="total">35</b></span></div>
			</div>
			<div class="pricePopup__row">
				<div class="pricePopup__desc"><span><b class="humans">2</b></span><img src="pic/popup-human.png"/><span>x&nbsp;&nbsp;<b class="count">3</b></span><img src="pic/popup-small-pizza.png"/><span>= £<b class="total">35</b></span></div>
			</div>
			<div class="pricePopup__row">
				<div class="pricePopup__desc"><span><b class="humans">2</b></span><img src="pic/popup-human.png"/><span>x&nbsp;&nbsp;<b class="count">3</b></span><img src="pic/popup-small-pizza.png"/><span>= £<b class="total">35</b></span></div>
			</div>
			<div class="pricePopup__row">
				<div class="pricePopup__desc"><span><b class="humans">2</b></span><img src="pic/popup-human.png"/><span>x&nbsp;&nbsp;<b class="count">3</b></span><img src="pic/popup-small-pizza.png"/><span>= £<b class="total">35</b></span></div>
			</div>
			<div class="pricePopup__row">
				<div class="pricePopup__desc"><span><b class="humans">2</b></span><img src="pic/popup-human.png"/><span>x&nbsp;&nbsp;<b class="count">3</b></span><img src="pic/popup-small-pizza.png"/><span>= £<b class="total">35</b></span></div>
			</div>
			<div class="pricePopup__row">
				<div class="pricePopup__desc"><span><b class="humans">2</b></span><img src="pic/popup-human.png"/><span>x&nbsp;&nbsp;<b class="count">3</b></span><img src="pic/popup-small-pizza.png"/><span>= £<b class="total">35</b></span></div>
			</div>
			<div class="pricePopup__row">
				<div class="pricePopup__desc"><span><b class="humans">2</b></span><img src="pic/popup-human.png"/><span>x&nbsp;&nbsp;<b class="count">3</b></span><img src="pic/popup-small-pizza.png"/><span>= £<b class="total">35</b></span></div>
			</div>
		</div>
	</div>
</div>