
<div class="mfiModal zoomAnim pricePopup pricePopup--party">
	<div class="regPopup__header">
		<div class="regPopup__logo">
			<div class="topWhiteLine__logo">
				<div class="topWhiteLine__logo__wrapper">
					<div class="topWhiteLine__logo__wrapper__border">
						<div class="svgHolder">
							<svg>
								<use xlink:href="#logo"/>
							</svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="regPopup__textBlock">
		<div class="regPopup__title mainTitle_green w_tac">Наши цены</div>
		<div class="pricePopup__partyBlock">
			<div class="pricePopup__partyBlock__row">
				<div class="pricePopup__partyBlock__images">
					<div class="pricePopup__partyBlock__half"><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/>
					</div><img src="pic/popup-big-pizza.png"/>
					<div class="pricePopup__partyBlock__half"><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/>
					</div>
				</div>
				<div class="pricePopup__partyBlock__desc"><b>4 x</b> минимальный заказ от <b>= £300</b></div>
			</div>
			<div class="pricePopup__partyBlock__row">
				<div class="pricePopup__partyBlock__images">
					<div class="pricePopup__partyBlock__half"><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/>
					</div><img src="pic/popup-big-pizza.png"/>
					<div class="pricePopup__partyBlock__half"><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/>
					</div>
				</div>
				<div class="pricePopup__partyBlock__desc"><b>6 x</b> минимальный заказ от <b>= £300</b></div>
			</div>
			<div class="pricePopup__partyBlock__row">
				<div class="pricePopup__partyBlock__images">
					<div class="pricePopup__partyBlock__half"><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/>
					</div><img src="pic/popup-big-pizza.png"/>
					<div class="pricePopup__partyBlock__half"><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/>
					</div>
				</div>
				<div class="pricePopup__partyBlock__desc"><b>8 x</b> минимальный заказ от <b>= £300</b></div>
			</div>
			<div class="pricePopup__partyBlock__row">
				<div class="pricePopup__partyBlock__images">
					<div class="pricePopup__partyBlock__half"><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/>
					</div><img src="pic/popup-big-pizza.png"/>
					<div class="pricePopup__partyBlock__half"><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/><img src="pic/popup-human.png"/>
					</div>
				</div>
				<div class="pricePopup__partyBlock__desc"><b>10 x</b> минимальный заказ от <b>= £300</b></div>
			</div>
		</div>
	</div>
</div>