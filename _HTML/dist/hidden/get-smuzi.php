
<div class="mfiModal zoomAnim pricePopup pricePopup--smuziBox">
	<div class="regPopup__header">
		<div class="regPopup__logo">
			<div class="topWhiteLine__logo">
				<div class="topWhiteLine__logo__wrapper">
					<div class="topWhiteLine__logo__wrapper__border">
						<div class="svgHolder">
							<svg>
								<use xlink:href="#logo"/>
							</svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="regPopup__textBlock">
		<div class="regPopup__title mainTitle_green w_tac">Закажите смузи</div>
		<div class="pricePopup__smuziBlock">
			<label>
				<input type="radio" name="count" checked="checked"/>
				<div class="checker"><span></span><b>2</b><img src="pic/popup-human.png"/><b>= £ 300</b></div>
			</label>
			<label>
				<input type="radio" name="count"/>
				<div class="checker"><span></span><b>4</b><img src="pic/popup-human.png"/><b>= £ 600</b></div>
			</label>
			<div class="pricePopup__smuziBlock__button"><a href="#" class="button_aqua button_semi">Оформить заказ</a></div>
		</div>
	</div>
</div>