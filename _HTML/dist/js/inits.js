/*init.js*/
/*
 init.js v2.0
 Wezom wTPL v4.0.0
 */
var $bigcalendar;
window.wHTML = (function ($) {

    /* Приватные переменные */

    var varSeoIframe = 'seoIframe',
        varSeoTxt = 'seoTxt',
        varSeoClone = 'cloneSeo',
        varSeoDelay = 200;

    /* Приватные функции */

    /* проверка типа данных на объект */
    var _isObject = function (data) {
            var flag = (typeof data == 'object') && (data + '' != 'null');
            return flag;
        },

    /* создание нового элемента элемента */
        _crtEl = function (tag, classes, attrs, jq) {
            var tagName = tag || 'div';
            var element = document.createElement(tagName);
            var jQueryElement = jq || true;
            // если классы объявлены - добавляем
            if (classes) {
                var tagClasses = classes.split(' ');
                for (var i = 0; i < tagClasses.length; i++) {
                    element.classList.add(tagClasses[i]);
                }
            }
            // если атрибуты объявлены - добавляем
            if (_isObject(attrs)) {
                for (var key in attrs) {
                    var val = attrs[key];
                    element[key] = val;
                }
            }
            // возвращаем созданый елемент
            if (jQueryElement) {
                return $(element);
            } else {
                return element;
            }
        },

    /* создаем iframe для сео текста */
        _seoBuild = function (wrapper) {
            var seoTimer;
            // создаем iframe, который будет следить за resize'm окна
            var iframe = _crtEl('iframe', false, {id: varSeoIframe, name: varSeoIframe});
            iframe.css({
                'position': 'absolute',
                'left': '0',
                'top': '0',
                'width': '100%',
                'height': '100%',
                'z-index': '-1'
            });
            // добавляем его в родитель сео текста
            wrapper.prepend(iframe);
            // "прослушка" ресайза
            seoIframe.onresize = function () {
                clearTimeout(seoTimer);
                seoTimer = setTimeout(function () {
                    wHTML.seoSet();
                }, varSeoDelay);
            };
            // вызываем seoSet()
            wHTML.seoSet();
        };

    /* Публичные методы */

    function Methods() {
    }

    Methods.prototype = {

        /* установка cео текста на странице */
        seoSet: function () {
            if ($('#' + varSeoTxt).length) {
                var seoText = $('#' + varSeoTxt);
                var iframe = seoText.children('#' + varSeoIframe);
                if (iframe.length) {
                    // если iframe сущствует устанавливаем на место сео текст
                    var seoClone = $('#' + varSeoClone);
                    if (seoClone.length) {
                        // клонеру задаем высоту
                        seoClone.height(seoText.outerHeight(true));
                        // тексту задаем позицию
                        seoText.css({
                            top: seoClone.offset().top
                        });
                    } else {
                        // клонера нету - бьем в колокола !!!
                        console.error('"' + varSeoClone + '" - не найден!');
                    }
                } else {
                    // если iframe отсутствует, создаем его и устанавливаем на место сео текст
                    _seoBuild(seoText);
                }
            }
        },

        /* magnificPopup inline */
        mfi: function () {
            $('.mfi').magnificPopup({
                type: 'inline',
                closeBtnInside: true,
                removalDelay: 300,
                mainClass: 'zoom-in'
            });
        },

        /* magnificPopup ajax */
        mfiAjax: function () {
            $('body').magnificPopup({
                delegate: '.mfiA',
                callbacks: {
                    elementParse: function (item) {
                        this.st.ajax.settings = {
                            url: item.el.data('url'),
                            type: 'POST',
                            data: (typeof item.el.data('param') !== 'undefined') ? item.el.data('param') : ''
                        };
                    },
                    ajaxContentAdded: function (el) {
                        wHTML.validation();
                        popupRating();
                        //datepicker + momentjs

                        if ($('.mfiModal .calendar input').length) {
                            var picker = new Pikaday({
                                field: $('.mfiModal .calendar input')[0],
                                format: "DD/MM/YYYY",
                                onSelect: function () {
                                    var date = moment(this._d).format('DD/MM/YYYY');
                                    $('.mfiModal .calendar input').val(date);
                                },
                                yearRange: [1930, 2000],
                                minDate: new Date('1930-01-01'),
                                maxDate: new Date('2000-12-31'),
                                firstDay: 1,
                                defaultDate: new Date('1990-01-01'),
                                i18n: wLocalize.pikaDay[wLang]
                            });
                        }

                        $('.js-phone-mask').inputmask({
                            "placeholder": "+37 (___) ___ __ __",
                            "mask": "+37 (999) 999 99 99"
                        });
                    }
                },
                type: 'ajax',
                removalDelay: 300,
                mainClass: 'zoom-in'
            });
        },

        /* оборачивание iframe и video для адаптации */
        wTxtIFRAME: function () {
            var list = $('.wTxt').find('iframe').add($('.wTxt').find('video'));
            if (list.length) {
                // в цикле для каждого
                for (var i = 0; i < list.length; i++) {
                    var element = list[i];
                    var jqElement = $(element);
                    // если имеет класс ignoreHolder, пропускаем
                    if (jqElement.hasClass('ignoreHolder')) {
                        continue;
                    }
                    if (typeof jqElement.data('wraped') === 'undefined') {
                        // определяем соотношение сторон
                        var ratio = parseFloat((+element.offsetHeight / +element.offsetWidth * 100).toFixed(2));
                        if (isNaN(ratio)) {
                            // страховка 16:9
                            ratio = 56.25;
                        }
                        // назнчаем дату и обрачиваем блоком
                        jqElement.data('wraped', true).wrap('<div class="iframeHolder ratio_' + ratio.toFixed(0) + '" style="padding-top:' + ratio + '%;""></div>');
                    }
                }
                // фиксим сео текст
                this.seoSet();
            }
        }
    };

    /* Объявление wHTML и базовые свойства */

    var wHTML = $.extend(true, Methods.prototype, {});

    return wHTML;

})(jQuery);

function wrapYoutube() {
    $('iframe[src*="youtube.com"]').wrap('<div class="youtube-wrap"></div>');
}

function sortNumber(a, b) {
    return a - b;
}

function popupRating() {
    $('.js-popupRecipe').each(function (index, el) {
        var count = parseInt($(el).data('rating'));
        $(el).find('.js-icon').slice(0, count).addClass('is-active');
    });


}

function progress() {
    if ($('.js-lkBox').length) {
        var el = $('.js-lkBox');
        var current = parseInt(el.data('rating'));
        var next = el.data('checks');
        var nexta = next.split(',');
        var nextlevel = 0;

        for (var i = 0, len = nexta.length; i < len; i++) {
            nexta[i] = parseInt(nexta[i], 10);
        }
        nexta.sort(sortNumber);

        for (var i = 0, len = nexta.length; i < len; i++) {
            if (nexta[i] > current) {
                nextlevel = nexta[i];
                break;
            }
        }

        var ost = nextlevel - current;
        $('.lkBlock__lineResult__status b em').text(ost);
    }
}

/**
 * функция для линейки в лк (прогресс)
 * @param {string} data-checks Передаем сюда номера засечек на шкале от 0 до 30
 * @param {string} data-text Передаем сюда текст этих засечек
 * @param {string} data-rating Передаем сюда текущий уровень пользователя
 */
function lkBox() {
    if ($('.js-lkBox').length) {
        var el = $('.js-lkBox');
        var counts = el.data('checks');
        var texts = el.data('text');
        var countList = counts.split(',');
        var countTextsList = texts.split(',');
        var rating = parseInt(el.data('rating'));

        $('.js-rang').slice(0, rating).addClass('colored');

        for (var i = 0, len = countList.length; i < len; i++) {
            countList[i] = parseInt(countList[i], 10);
        }

        $('.js-rang').each(function (index, el) {
            var ind = countList[index];
            if (ind != 0) {
                var ii = 30 - ind;
                if (jQuery.inArray(ind, countList) != -1) {
                    $('.js-rang').eq(ind - 1).attr('data-count', ind);
                    $('.js-rang').eq(ind - 1).attr('data-countText', countTextsList[index]);
                    $('.js-rang').eq(ind - 1).addClass('issetCheck');
                }
            } else {
                $('.js-rang').eq(0).attr('data-count', ind);
                $('.js-rang').eq(0).attr('data-countText', countTextsList[index]);
                $('.js-rang').eq(0).addClass('issetCheck left');
            }
        });
    }
}

function top_menu() {
    $('.topWhiteLine__menu > ul > li, .topWhiteLine__menu > ul > li > ul > li').each(function (index, el) {
        if ($(el).children('ul').length) {
            $(el).addClass('hasChild');
            $(el).addClass('js-drop');
            $(el).children('ul').wrapAll('<div class="wrap"></div>');
        }
    });
}

/**
 * выделение активного таба при загрузке страницы на странице смузи
 */
function tabs() {
    var src = $('.js-smuzi-tab.is-active').data('src');
    var dist = $('.js-smuzi-dist[data-dist="' + src + '"]');
    var bg = $('.js-smuzi-tab.is-active').data('bg');
    $('.tabsVitamins').css('background-image', 'url(' + bg + ')');
    dist.show();
}

/**
 * выделение активного таба
 */
function universaltabs() {
    $('.js-tab').each(function (index, el) {
        var src = $(el).parent().find('.is-active').data('src');
        var dist = $('.js-dist[data-dist="' + src + '"]');
        dist.show();
    });

}

function onCreate(e) {
    wHTML.seoSet();
    $('.howBlock__slide__item').each(function (index, el) {
        $(el).find('.howWeWork__step__title i').text(index + 1);
    });
}



function onCreate2(e) {
    wHTML.seoSet();
    $('.howBlock__slide').show();
    $('.howBlock__slide').trigger('updateSizes');
    $('.sliderPreloader').fadeOut();
    $('.howBlock__slide__item').each(function (index, el) {
        $(el).find('.howWeWork__step__title i').text(index + 1);
    });
    $('.howBlock__slide__item').eq(0).addClass('aanimate');

}


function mmenu() {
    $('.topWhiteLine__menu > ul').clone().prependTo('#mmenu ul');
    $('.mm-panels').find('.topWhiteLine__menu').removeClass('topWhiteLine__menu');
}

jQuery(document).ready(function ($) {
    var countSlides;
    var centerSlide;
    var $car;

    $('.js-phone-mask').inputmask({
        "placeholder": "+37 (___) ___ __ __",
        "mask": "+37 (999) 999 99 99"
    });

    if ($('.bigcalendar').length) {
        $bigcalendar = $('.bigcalendar').pickadate({
            monthsFull: wLocalize.pikaDay[wLang].months,
            weekdaysShort: wLocalize.pikaDay[wLang].weekdaysShort,
            showMonthsShort: undefined,
            showWeekdaysFull: undefined,

            firstDay: 0,
            min: true
        });

    }

    var vid = document.getElementById("video");
    /**
     * клик по кнопке запуска видео в шапке
     */
    $('.js-play').on('click', function (e) {
        e.preventDefault();
        var el = $(this);
        if (el.hasClass('is-active')) {
            el.removeClass('is-active');
            vid.pause();
            $('#video').hide();
            $('.bigImage__title,.bigImage__desc').show();
            $('.js-play').html('&#9658;');
        }
        else {
            el.addClass('is-active');
            vid.play();
            $('#video').show();
            $('.bigImage__title,.bigImage__desc').hide();
            $('.js-play').html('&#10074;&#10074;');
        }
    });

    $('.recipesBlock__item:not(.recipesBlock--big)').matchHeight({
        byRow: false
    });

    var $gridFaq = $('.faq__blocks').isotope({
        itemSelector: '.faq__block',
        transitionDuration: '0.4s',
        layoutMode: 'packery',
        packery: {
            gutter: 0
        }
    });

    var $grid2 = $('.recipesBlock__smuzis').isotope({
        itemSelector: '.smuzis',
        transitionDuration: '0.8s',
        layoutMode: 'packery',
        packery: {
            gutter: 0
        }
    });


    var $grid = $('.recipesBlock__items').isotope({
        itemSelector: '.recipesBlock__item',
        transitionDuration: '0.8s',
        layoutMode: 'packery',
        packery: {
            gutter: 0
        }
    });


    $('.recipesBlock__optionsLine__filters').on('click', '.js-filter-button', function () {
        var filterValue = $(this).attr('data-filter');
        if ($('.recipesBlock__items').length) {
            $grid.isotope({filter: filterValue});
        }

        if ($('.recipesBlock__smuzis').length) {
            $grid2.isotope({filter: filterValue});
        }

        $('.recipesBlock__optionsLine__filters').find('.is-active').removeClass('is-active');
        $(this).addClass('is-active');

    });


    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span class="text-select">' + state.text + '<b class="people-select-icon"></b></span>'
        );
        return $state;
    };

    function template(data, container) {
        var $state2 = $(
            '<span class="text-select">' + data.text + '<b class="people-select-icon"></b></span>'
        );
        return $state2;
    }

    $('.ourBlock__buyBottomLine__peoples__select').select2({
        minimumResultsForSearch: -1,
        width: 160,
        templateResult: formatState,
        templateSelection: template
    });

    if ($('#carousel').length) {
        countSlides = $('#carousel').find('.sslide').length;
        if (countSlides % 2 == 0) {
            centerSlide = countSlides / 2;
        } else {
            centerSlide = Math.ceil(countSlides / 2);
        }
    }

    if ($('.calendar input').length) {
        var picker = new Pikaday({
            field: $('.calendar input')[0],
            format: "DD/MM/YYYY",
            onSelect: function () {
                var date = moment(this._d).format('DD/MM/YYYY');
                $('.calendar input').val(date);
            },
            yearRange: [1930, 2000],
            minDate: new Date('1930-01-01'),
            maxDate: new Date('2000-12-31'),
            firstDay: 0,
            defaultDate: new Date('1990-01-01'),
            i18n: wLocalize.pikaDay[wLang]
        });
    }

    $('.animation').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('animate');
        } else {
            $(this).removeClass('animate');
        }
    });

    $('.accordion').liHarmonica({
        currentClass: 'is-active',//Класс для выделенного пункта меню
        onlyOne: true,      //true - открытым может быть только один пункт,
                            //false - число открытых одновременно пунктов не ограничено
        speed: 500          //Скорость анимации
    });

    tabs();

    /** underscore module for add item to basket on page recipe.html
     *
    var globalArrayObjects = [];

    var template = $('#item').html();

    $('.js-add-item').on('click', function () {
        var el = $(this).closest('.js-item');
        var array = [];
        var id = el.data('id');
        var name = el.data('name');
        var type = el.data('type');
        var price = el.data('price');
        var count = 2;
        if (globalArrayObjects.length < 4) {
            array.push(id, name, type, count, price);
            var items = _.object(['id', 'name', 'type', 'count', 'price'], array);

            var globalPorcies = parseInt($('input[name=countPorcies]:checked').attr('value'));
            var max = globalPorcies * 4;
            var inarray = _.reduce(globalArrayObjects, function (memo, num) {
                return memo + num.count;
            }, 0);


            if (_.findWhere(globalArrayObjects, {id: id}) == undefined && inarray < max) {
                globalArrayObjects.push(items);
            } else {
                checkCountInCart(id);
            }
            reRender();
        }

    });

    function totalWithoutPromo() {
        var total = _.reduce(globalArrayObjects, function (memo, num) {
            return memo + num.count * num.price;
        }, 0);
        total = total.toFixed(2);
        $('.js-totalWithoutPromo').text(total);
    }

    function reRender() {
        var compiled = _.template(template);
        var res = compiled({items: globalArrayObjects});
        $('.recipesBlock__shortView__items').html(res);
        totalWithoutPromo();
    }

    function checkCountInCart(id) {
        var globalPorcies = parseInt($('input[name=countPorcies]:checked').attr('value'));
        var max = globalPorcies * 4;

        var inarray = _.reduce(globalArrayObjects, function (memo, num) {
            return memo + num.count;
        }, 0);

        if (inarray < max) {
            var ll = _.where(globalArrayObjects, {id: id});
            ll[0].count = ll[0].count + 2;
        }
        reRender();
    }

    $('body').on('click', '.js-add-porcies', function () {
        checkCountInCart(parseInt($(this).closest('.recipesBlock__shortView__item').data('id')));
    });

    $('.recipesBlock__shortView__inps label').on('click', function () {
        var globalPorcies = parseInt($('input[name=countPorcies]:checked').attr('value'));
        var max = globalPorcies * 4;
        var inarray = _.reduce(globalArrayObjects, function (memo, num) {
            return memo + num.count;
        }, 0);

        if (inarray > max) {
            _.map(globalArrayObjects, function (num, key) {
                num.count = 2;
                return num;
            });
            reRender();
        }
    });

    $('body').on('click', '.js-remove', function () {
        var el = $(this);
        var id = parseInt(el.closest('.recipesBlock__shortView__item').data('id'));
        var obj = _.findWhere(globalArrayObjects, {id: id});
        obj.count += -2;

        globalArrayObjects = _.reject(globalArrayObjects, function (num) {
            return num.count <= 0;
        });
        reRender();
    });

    */

    $('.js-drop').on('click', function () {
        var el = $(this);
        if (el.hasClass('is-active')) {
            el.removeClass('is-active');
            el.next('ul').stop().slideUp(300,function(){
                $gridFaq.isotope('reloadItems').isotope();
            });
        } else {
            el.addClass('is-active');
            el.next('ul').stop().slideDown(300,function(){
                $gridFaq.isotope('reloadItems').isotope();
            });
        }

    });

    $('body').on('click', '.calendar .svgHolder', function () {
        var el = $(this);
        el.closest('.calendar').find('input').trigger('click');
    });

    $('.topWhiteLine__menu > ul').on('mouseover', '.js-drop', function () {
        var el = $(this);
        el.children('.wrap').addClass('drop');
    });

    $('.topWhiteLine__menu > ul').on('mouseout', '.js-drop', function () {
        var el = $(this);
        el.children('.wrap').removeClass('drop');
    });

    /**
     * клик по кнопке показать ингредиенты
     */
    $('.js-show-ingredients').on('click', function () {
        var el = $(this);
        if (el.hasClass('is-active')) {
            el.removeClass('is-active');
            el.closest('.smuzis').find('.ourBlock__composition').removeClass('drop');
        } else {
            el.addClass('is-active');
            el.closest('.smuzis').find('.ourBlock__composition').addClass('drop');
        }
        $grid2.isotope('reloadItems').isotope();
    });

    /**
     * клик по иконке табов на странице смузи
     */
    $('.js-smuzi-tab').on('click', function () {
        var el = $(this);
        var current_src = $('.js-smuzi-tab.is-active').data('src');
        var current_dist = $('.js-smuzi-dist[data-dist="' + current_src + '"]');
        var src = el.data('src');
        var bg = el.data('bg');
        var dist = $('.js-smuzi-dist[data-dist="' + src + '"]');
        $('.tabsVitamins').removeClass(current_src);
        $('.tabsVitamins').addClass('animate');
        $('.tabsVitamins').css('background-image', 'url(' + bg + ')');
        $(current_dist).stop().fadeOut(600, function () {
            $(dist).stop().fadeIn(600, function () {
                $('.tabsVitamins').removeClass('animate');
            });
        });

        $('.js-smuzi-tab').removeClass('is-active');
        el.addClass('is-active');
    });

    /**
     * клик по кнопке универсального таба
     */
    $('.js-tab').on('click', function () {
        var el = $(this);
        var current_src = el.parent().find('.is-active').data('src');
        var current_dist = $('.js-dist[data-dist="' + current_src + '"]');
        var src = el.data('src');
        var dist = $('.js-dist[data-dist="' + src + '"]');
        $(current_dist).stop().fadeOut(4, function () {
            $(dist).stop().fadeIn();
        });
        el.parent().find('.is-active').removeClass('is-active');
        el.addClass('is-active');
        if(src == 'password') {
            $('.js-edit').hide();
        } else{
            if($('.lkBlock__userInfo__col input').hasClass('notEdit')) {
                $('.js-edit').show();
            }
        }
    });

    $('.js-edit').on('click',function(e){
        e.preventDefault();
        var picker2;
        $('.js-calendar input').pikaday({
            format: "DD/MM/YYYY",
            onSelect: function () {
                var date = moment(this._d).format('DD/MM/YYYY');
                $('.js-calendar input').val(date);
            },
            yearRange: [1930, 2000],
            minDate: new Date('1930-01-01'),
            maxDate: new Date('2000-12-31'),
            firstDay: 0,
            defaultDate: new Date('1990-01-01'),
            i18n: wLocalize.pikaDay[wLang]
        });

        var el = $(this);
        if (el.hasClass('is-active')){
            $('.js-submit').hide();
            el.removeClass('is-active');
            $('.lkBlock__userInfo__col input').addClass('notEdit');
            $('.lkBlock__userInfo__col select').addClass('notEdit');

            $('.js-calendar input').pikaday('destroy');

        } else{
            $('.js-submit').show();
            el.addClass('is-active');
            $('.notEdit').removeAttr('readonly');
            $('.notEdit').removeAttr('disabled');
            $('.notEdit').removeClass('notEdit');
            $('.js-edit').hide();


        }
    });

    $('.js-step').on('click', function () {
        var el = $(this);
        var form = el.closest('.wForm');
        if (form.valid() === true) {
            $('.js-first').slideUp(400, function () {
                $('.js-second').slideDown();
            });
        }
    });

    // поддержка cssanimations
    transitFlag = Modernizr.cssanimations;

    // очитска localStorage
    localStorage.clear();

    // сео текст
    wHTML.seoSet();

    // magnificPopup inline
    wHTML.mfi();

    // magnificPopup ajax
    wHTML.mfiAjax();

    // валидация форм
    wHTML.validation();

    mmenu();
    top_menu();
    wrapYoutube();
    lkBox();
    progress();
    universaltabs();

    $("#mmenu").mmenu({});
    $('.countdown').countdown('2016/10/10', function (event) {
        $(this).html(event.strftime('<span data-name="дней">%D</span><span data-name="часов">%H</span><span data-name="минут">%M</span><span data-name="секунд" class="red">%S</span>'));
    });
    $('.ourPackage__textBlock').matchHeight();
    $(window).load(function () {
        // оборачивание iframe и video для адаптации
        wHTML.wTxtIFRAME();

        // $('.rotateslider-container').slick({
        //     centerMode: true,
        //     centerPadding: '10px',
        //     slidesToShow: 5
        // });

        // function onBeforef(e) {
        //     wHTML.seoSet();
        //     $('.howBlock__slide__item').removeClass('animate');
        // }

        function onAfterf(e) {
            wHTML.seoSet();
            $('.howBlock__slide__item').removeClass('aanimate');
            e.items.visible.eq(0).addClass('aanimate');
        }

        $('.howBlock__slide').carouFredSel({
            circular: true,
            infinite: true,
            responsive: true,
            height: 'variable',
            width: '100%',
            items: {
                height: 'variable',
                visible: {
                    min: 1,
                    max: 1
                }
            },
            scroll: {
                items: 1,
                fx: "crossfade",
                //onBefore: onBeforef,
                onAfter:onAfterf
            },
            auto: {
                play: false,
            },
            prev: $('#prev'),
            next: $('#next'),
            pagination: $('.paginationSlide'),
            swipe: {
                onMouse: false,
                onTouch: true
            },
            onCreate: onCreate2,
        }, {
            transition: transitFlag
        });


        $car = $('#carousel');
        // for (var a = 0; a < 3; a++) {
        //     $car.prepend('<div />');
        // }
        // for (var a = 0; a < 3; a++) {
        //     $car.append('<div />');
        // }

        $car.find('.weekSlider__block').css('zIndex', 1);
        $car.find('.weekSlider__block').eq(0).css('zIndex', 2);

        $car.carouFredSel({
            circular: true,
            infinite: false,
            responsive: false,
            width: '100%',
            height: 250,
            items: {
                width: 'variable',
                start: centerSlide,
                visible: {
                    min: 7
                }
            },
            fx: 'fade',
            auto: false,
            scroll: {
                items: 1,
                duration: 1500,
                onBefore: function (data) {

                    data.items.old.eq(1).removeClass('sm left right');
                    data.items.old.eq(2).removeClass('md left right');
                    data.items.old.eq(5).removeClass('sm left right');
                    data.items.old.eq(4).removeClass('md left right');

                    data.items.visible.eq(1).addClass('sm left').css('zIndex', 0);
                    data.items.visible.eq(2).addClass('md left').css('zIndex', 1);
                    data.items.visible.eq(5).addClass('sm right').css('zIndex', 0);
                    data.items.visible.eq(4).addClass('md right').css('zIndex', 1);

                    data.items.old.eq(3).find('.weekSlider__block').css('zIndex', 1);
                    data.items.visible.eq(3).find('.weekSlider__block').css('zIndex', 2);
                    data.items.old.eq(3).removeClass('activeS');
                    data.items.visible.eq(3).addClass('activeS');


                    $('.sslide').each(function (index, el) {
                        if (!$(el).hasClass('sm') && !$(el).hasClass('md') && !$(el).hasClass('activeS')) {
                            $(el).addClass('not');
                        } else {
                            $(el).removeClass('not');
                        }
                    });

                },
                onAfter: function (data) {
                    $car.trigger('updateSizes');
                }
            },
            prev: $('#prevSlide'),
            next: $('#nextSlide'),
            swipe: {
                onMouse: true,
                onTouch: true
            },
            onCreate: function () {
                wHTML.seoSet();
                setTimeout(function(){
                    $car.trigger('updateSizes');
                    $('.sliderPreloader').fadeOut();
                },950);
            }
        }, {
            transition: transitFlag
        });

        $car.children().click(function () {
            var ht = $car.triggerHandler("currentPosition");
            var thh = $(this).attr('id');
            var thhS = thh.split('-')[1];

            $car.trigger('slideTo', [thhS - 1]);
        });



    });

    $(window).resize(function(){
        $('.sliderPreloader').show();
        setTimeout(function(){
            $car.trigger('updateSizes');
            $('.sliderPreloader').fadeOut(200);
        },950);
    });
});
//# sourceMappingURL=maps/inits.js.map
