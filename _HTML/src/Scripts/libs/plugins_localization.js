﻿/*
    wLang v1.0 / 20.11.2015
    Studio WEZOM / Oleg Dutchenko
    Wezom wTPL v4.0.0
*/

window.wLang = (function(window, document, undefined) {
	// массив имеющихся локализации
		var names = ['ru', 'lt', 'en'];
	// флаг отсутсутвующей локализации
		var miss = true;
	// язык по умолчанию
		var def = 'ru';
	// навигатор объекта Window
		var navi = window.navigator;
	// определяем язык документа
		var lang = document.documentElement.getAttribute('lang') ||
			navi.language ||
			navi.browserLanguage ||
			w.userLanguage || def;

	// от кода языка отсекаем код региона(если присутствует) и переводим в нижний регистр
		lang = lang.toLowerCase().substr(0, 2);

	// сверяем полученый результат с имеющимся массивом
		for (var i = 0; i < names.length; i++) {
			if (lang === names[i]) {
				miss = false;
				break;
			}
		}

	// если получаем отсутствующий вариант определяем язык по умолчанию
		if (miss) {
			lang = def;
		}

	// возвращает язык в качестве нового свойтства объекту Window
	// далее свойтво можно использавать как значение глобальной переменной wLang
	// console.log(wLang);
		return lang;

})(this, this.document);

/*
    wLocalize v1.0 / 20.11.2015
    Studio WEZOM / Oleg Dutchenko
*/

window.wLocalize = (function(window, document, undefined) {
	// объект содержащий переводы
		var localize = {};

    // переводы для плагина jquery.magnific-popup
        localize.Magnific = {
            ru: {
                tClose: 'Закрыть (ESC)',
                tLoading: 'Загрузка контента ...',
                tNotFound: 'Контент не найден',
                tError: 'Невозможно загрузить <a href="%url%" target="_blank">Контент</a>.',
                tErrorImage: 'Невозможно загрузить <a href="%url%" target="_blank">Изображение #%curr%</a>.',
                tPrev: 'Предыдущая (клавиша Left)',
                tNext: 'Следующая (клавиша Right)',
                tCounter: '%curr% из %total%'
            },
            en: {
                tClose: 'Close (ESC)',
                tLoading: 'Loading ...',
                tNotFound: 'Content not found',
                tError: '<a href="%url%" target="_blank">The content</a> could not be loaded.',
                tErrorImage: '<a href="%url%" target="_blank">The image #%curr%</a> could not be loaded.',
                tPrev: 'Previous (Left arrow key)',
                tNext: 'Next (Right arrow key)',
                tCounter: '%curr% of %total%'
            },
            lt:{
                tClose: 'Uždaryti sistema (ESC)',
                tLoading: 'Įkeliama ...',
                tNotFound: 'turinys nerastas',
                tError: '<a href="%url%" target="_blank"> turinys </a> negali būti įkeltas. =',
                tErrorImage: '<a href="%url%" target="_blank"> vaizdas #%curr% </a>.',
                tPrev: 'Ankstesnis (Kairysis rodyklės klavišą)',
                tNext: 'Kitas (Teisė rodyklės klavišą)',
                tCounter: '%curr% iš% %total%'
            }
        };

    // переводы для плагина jquery-validation
        localize.Validate = {
            ru: {
                required: "Это поле необходимо заполнить!",
                required_checker: "Этот параметр - обязателен!",
                required_select: "Этот параметр - обязателен!",
                password: "Пароль должен быть не короче 6 символов и содержать цифры и буквы",
                remote: "Пожалуйста, введите правильное значение!",
                email: "Пожалуйста, введите корректный адрес электронной почты!",
                url: "Пожалуйста, введите корректный URL!",
                date: "Пожалуйста, введите корректную дату!",
                dateISO: "Пожалуйста, введите корректную дату в формате ISO!",
                number: "Пожалуйста, введите число!",
                digits: "Пожалуйста, вводите только цифры!",
                creditcard: "Пожалуйста, введите правильный номер кредитной карты!",
                equalTo: "Пожалуйста, введите такое же значение ещё раз!",
                maxlength: "Пожалуйста, введите не более {0} символов!",
                maxlength_checker: "Пожалуйста, выберите не более {0} параметров!",
                maxlength_select: "Пожалуйста, выберите не более {0} пунктов!",
                minlength: "Пожалуйста, введите не менее {0} символов!",
                minlength_checker: "Пожалуйста, выберите не менее {0} параметров!",
                minlength_select: "Пожалуйста, выберите не менее {0} пунктов!",
                rangelength: "Пожалуйста, введите значение длиной от {0} до {1} символов!",
                rangelength_checker: "Пожалуйста, выберите от {0} до {1} параметров!",
                rangelength_select: "Пожалуйста, выберите от {0} до {1} пунктов!",
                range: "Пожалуйста, укажите значение от {0} до {1}!",
                filetype: "Допустимые расширения файлов: {0}!",
                filesize: "Максимальный объем {0}KB!",
                filesizeEach: "Максимальный объем каждого файла {0}KB!",
                max: "Пожалуйста, укажите значение, меньше или равное {0}!",
                min: "Пожалуйста, укажите значение, больше или равное {0}!",
                pattern: "Укажите значение соответствующее маске {0}!",
                // add
                word: "Введите корректное словесное значение!",
                login: "Введите корректный логин!",
                phoneUA: "Некорректный формат номера",
                mydate: "Введите верную дату в формате ДД/ММ/ГГГГ"
            },
            en: {
                required: "This field is required!",
                required_checker: "This parameter is required!",
                required_select: "This parameter is required!",
                password: "Specify paroll!",
                remote: "Please fix this field!",
                email: "Please enter a valid email address!",
                url: "Please enter a valid URL!",
                date: "Please enter a valid date!",
                dateISO: "Please enter a valid date ISO!",
                number: "Please enter a valid number!",
                digits: "Please enter only digits!",
                creditcard: "Please enter a valid credit card number!",
                equalTo: "Please enter the same value again!",
                maxlength: "Please enter no more than {0} characters!",
                maxlength_checker: "Please select no more than {0} parameters!",
                maxlength_select: "Please select no more than {0} items!",
                minlength: "Please enter at least {0} characters!",
                minlength_checker: "Please select at least {0} options!",
                minlength_select: "Please select at least {0} items!",
                rangelength: "Please enter a value between {0} and {1} characters long!",
                rangelength_checker: "Please select from {0} to {1} options!",
                rangelength_select: "Please select from {0} to {1} items!",
                range: "Please enter a value between {0} and {1}!",
                filetype: "Allowed file extensions: {0}!",
                filesize: "Maximum size {0}KB!",
                filesizeEach: "Maximum amount of each file {0}KB!",
                max: "Please enter a value less than or equal to {0}!",
                min: "Please enter a value greater than or equal to {0}!",
                pattern: "Specify a value corresponding to the mask {0}!",
                // add
                word: "Please enter the correct word meanings!",
                login: "Please enter a valid username!",
                phoneUA: "Invalid phone number format",
                mydate: "Please enter a valid date DD/MM/YYYY"
            },
            lt: {
                required: "Šis laukas yra privalomas.",
                remote: "Prašau pataisyti šį lauką.",
                email: "Prašau įvesti teisingą elektroninio pašto adresą.",
                url: "Prašau įvesti teisingą URL.",
                date: "Prašau įvesti teisingą datą.",
                dateISO: "Prašau įvesti teisingą datą (ISO).",
                number: "Prašau įvesti teisingą skaičių.",
                digits: "Prašau naudoti tik skaitmenis.",
                creditcard: "Prašau įvesti teisingą kreditinės kortelės numerį.",
                equalTo: "Prašau įvestį tą pačią reikšmę dar kartą.",
                extension: "Prašau įvesti reikšmę su teisingu plėtiniu.",
                maxlength: "Prašau įvesti ne daugiau kaip {0} simbolių.",
                minlength: "Prašau įvesti bent {0} simbolius.",
                rangelength: "Prašau įvesti reikšmes, kurių ilgis nuo {0} iki {1} simbolių." ,
                range: "Prašau įvesti reikšmę intervale nuo {0} iki {1}." ,
                max: "Prašau įvesti reikšmę mažesnę arba lygią {0}." ,
                min: "Prašau įvesti reikšmę didesnę arba lygią {0}." ,
                phoneUA: "Neteisingas telefono numeris formatas",
                mydate:"Prašome Įveskite-Walid ir datą DD/MM/YYYY"
            }
        };


        localize.pikaDay = {
            ru:{
                previousMonth : 'Предыдущий месяц',
                nextMonth     : 'Следующий месяц',
                months        : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                weekdays      : ['Понедельник','Вторник','Среда','Четверг','Пятница','Суббота','Воскресенье'],
                weekdaysShort : ['Пн','Вт','Ср','Чт','Пт','Сб','Вс']

            },
            lt:{
                previousMonth: 'Ankstesnis mėnuo',
                nextMonth: 'Kitas mėnuo',
                months: ['Sausis', 'Vasaris', 'Kovas', 'Balandžio', 'Gali', 'Birželis', 'Liepa', 'Rugpjūčio', 'Rugsėjo', 'Spalis', 'Lapkritis', 'Gruodis'],
                weekdays: ['Pirmadienis', 'Antradienis', 'Trečiadienis', 'Ketvirtadienis', 'Penktadienis', 'Šeštadienis', 'Sekmadienis'],
                weekdaysShort: ['PI', 'AN', 'TR', 'KE', 'PE', 'ŠE', 'SE']
            }
        };
    

	// возвращает объект с переводам в качестве нового свойтства объекту Window
	// далее объект можно использавать как значение глобальной переменной wLocalize
	// console.log(wLocalize);

        return localize;

	// пример использования
	// переведенный текст загрузки плагина MagnificPopup
	// console.log(wLocalize.Magnific[wLang].tLoading);

})(this, this.document);
